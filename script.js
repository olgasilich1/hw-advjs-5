// Асинхронность - это когда создание события и его обработка разделены во времени

const button = document.querySelector('#button');
const ipAdress = document.querySelector('.ipAdress');

button.addEventListener('click', async function (e) {
  e.preventDefault();
  const res = await fetch('https://api.ipify.org/?format=json');
  const d = await res.json();

  const resolve = await fetch(
    `http://ip-api.com/json/${d.ip}?fields=continent,country,region,city,district`
  );
  const data = await resolve.json();

  const names = ['continent', 'country', 'region', 'city', 'district'];

  for (let i = 0; i < names.length; i++) {
    createElement(names[i], data[`${names[i]}`]);
  }
});

function createElement(name, source) {
  const p = document.createElement('p');
  p.textContent = `${name}: ${source}; `;
  ipAdress.appendChild(p);
}
